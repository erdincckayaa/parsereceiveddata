/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  * das dsa 
  ******************************************************************************
  */ 
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <string.h>

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
int fputc(int ch, FILE *f)
{
  HAL_UART_Transmit(&huart4, (uint8_t *)&ch, 1, 1000);
  return ch;
}

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

#define RECEIVE_BUFFER_SIZE 1024
#define ONE_BYTE_PERIOD 0.00000868
#define HEADER_BYTE_SIZE 5
#define DELAY_TOLERANCE 10

enum
{
  data_INFO,
  data_WARNING,
  data_ERROR
} infoTypes;

enum
{
  START_TOKEN,
  DATA_SIZE,
  DATA_INFO_TYPE,
  DATA_COMMAND_TYPE,
  DATA
} m_step = START_TOKEN;

//receive buffer related // UART2
static uint8_t m_receiveData;
uint8_t m_ReceiveBuffer[RECEIVE_BUFFER_SIZE];
uint16_t m_ReceiveBufferCnt = 0;

uint8_t m_rxOkFlg = 0;

//header variables
uint16_t m_length = 0;
uint8_t m_infoType;
uint8_t m_commandType;



static uint32_t counter = 0;

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  HAL_UART_Receive_IT(&huart2, &m_receiveData, 1);

  //Veriyi alirken tekrar veri gelirse kabul etme
  if (m_rxOkFlg)
    return;

  switch (m_step)
  {
  case START_TOKEN:
    
    if (m_receiveData == 0x1A)
    {
      m_ReceiveBufferCnt = 0;
      m_step = DATA_SIZE;
    }
    break;

  case DATA_SIZE:
    //*((uint8_t *)&m_length + m_ReceiveBufferCnt++) = m_receiveData;
    m_ReceiveBuffer[m_ReceiveBufferCnt++] = m_receiveData;

    if (m_ReceiveBufferCnt >= 2)
    {
      m_length = m_ReceiveBuffer[0];
      m_length <<= 8U;  
      m_length |= m_ReceiveBuffer[1];


      //m_length = *((uint16_t *)(m_ReceiveBuffer));

      if (m_length <= RECEIVE_BUFFER_SIZE)
      {
        m_step = DATA_INFO_TYPE;
      }
      else
        m_step = START_TOKEN;
    }
    break;

  case DATA_INFO_TYPE:
    if (m_receiveData < 3)
    {
      m_ReceiveBuffer[m_ReceiveBufferCnt++] = m_receiveData;
      m_step = DATA_COMMAND_TYPE;
    }
    else
      m_step = START_TOKEN;
    break;

  case DATA_COMMAND_TYPE:
    if (m_receiveData == 1)
    {
      m_ReceiveBuffer[m_ReceiveBufferCnt++] = m_receiveData;

      //Initialize
      m_infoType = m_ReceiveBuffer[2];
      m_commandType = m_ReceiveBuffer[3];
      m_ReceiveBufferCnt = 0;
      m_step = DATA;
      counter = __HAL_TIM_GetCounter(&htim2);
    }
    else
      m_step = START_TOKEN;
    break;

  case DATA:
    if (__HAL_TIM_GetCounter(&htim2) - counter >= 500) 
    {
      m_step = START_TOKEN;
      
      //is it neccessary to do this ?
      for (int i = 0; i < 5; ++i) {
        m_ReceiveBuffer[i] = 0;
      }
      break;
    }
    if (m_ReceiveBufferCnt < m_length)
    {
      m_ReceiveBuffer[m_ReceiveBufferCnt++] = m_receiveData;
    }

    if (m_ReceiveBufferCnt == m_length )
    {
      m_rxOkFlg = 1;
      m_ReceiveBuffer[m_ReceiveBufferCnt] = '\0';
      m_step = START_TOKEN;
    }
  }
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_UART4_Init();
  MX_USART2_UART_Init();
  MX_TIM2_Init();
  /* USER CODE BEGIN 2 */


  HAL_UART_Receive_IT(&huart2, &m_receiveData, 1);
  HAL_TIM_Base_Start(&htim2);
  printf("\n\rSystem initilazing..\n\r");

  
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
    if (m_rxOkFlg)
    {
      //memcpy(printfBuf, m_ReceiveBuffer, m_length);

      printf("\n\r**Data has been received**\n\r");

      switch (m_infoType)
      {
      case data_WARNING:
        printf("[WARNING][%s]\n", m_ReceiveBuffer);
        break;
      case data_INFO:
        printf("[INFO][%s]\n", m_ReceiveBuffer);
        break;
      case data_ERROR:
        printf("[ERROR][%s]\n", m_ReceiveBuffer);
        break;
      }

      memset(m_ReceiveBuffer, 0, sizeof(m_ReceiveBuffer));
      m_rxOkFlg = 0;
      m_step = START_TOKEN;
    }
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 64;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV2;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  if (USART2->SR & (0x1F))
  {
    USART2->CR1 &= ~(0x1 << 4);
    USART2->CR1 &= ~(0x1 << 5);
    USART2->CR1 &= ~(0x1 << 8);
    USART2->CR3 &= ~(0x1 << 0);
    return;
  }
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

